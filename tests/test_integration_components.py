# Copyright (c) 2023 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Integration tests for components"""
from textwrap import dedent

from tests.test_integration import IntegrationTests
from tests.test_integration import assets_mkdir
from tests.test_integration import kpet_component_list
from tests.test_integration import kpet_run_generate
from tests.test_integration import kpet_test_list


class IntegrationComponentsTests(IntegrationTests):
    """Tests for components"""

    def test_supported(self):
        """
        Check the (missing) "supported" field has the right effect
        """
        assets = {
            "index.yaml": """
                template: output.txt.j2
                arches:
                  - arch
                  - arch2
                trees:
                  tree: {}
                  tree2: {}
                host_types:
                  normal: {}
                components:
                  universal:
                    description: A universally-supported component
                  arch_only:
                    description: Arch-only component
                    supported:
                      arches: arch
                  tree_only:
                    description: Tree-only component
                    supported:
                      trees: tree
                  arch2_tree2_only:
                    description: Arch2&Tree2-only component
                    supported:
                      arches: arch2
                      trees: tree2
                  exclusive1:
                    description: Component conflicting with exclusive2
                    supported:
                      components:
                        not: exclusive2
                  exclusive2:
                    description: Component conflicting with exclusive1
                    supported:
                      components:
                        not: exclusive1
                  dependent:
                    description: Component depending on universal
                    supported:
                      components: universal
                case:
                  universal_id: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
                  cases:
                    general:
                        name: general
                    arch_only:
                        name: arch_only
                        enabled:
                          components: arch_only
                    dependent:
                        name: dependent
                        enabled:
                          components: dependent
                    exclusive2:
                        name: exclusive2
                        enabled:
                          components: exclusive2
            """,
            "output.txt.j2": dedent("""\
                {%- for scene in SCENES -%}
                  {%- for recipeset in scene.recipesets -%}
                    {%- for HOST in recipeset -%}
                      {%- for test in HOST.tests -%}
                        {{- test.name + "\\n" -}}
                      {%- endfor -%}
                    {%- endfor -%}
                  {%- endfor -%}
                {%- endfor -%}
            """)
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_component_list, db_path,
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    dependent        Component depending on universal
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-c", "",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-c.*",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    dependent        Component depending on universal
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-ttree",
                status=0,
                stdout_equals=dedent("""\
                    arch_only        Arch-only component
                    dependent        Component depending on universal
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-ttree2",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    dependent        Component depending on universal
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-aarch",
                status=0,
                stdout_equals=dedent("""\
                    arch_only        Arch-only component
                    dependent        Component depending on universal
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-aarch2",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    dependent        Component depending on universal
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-aarch2", "-ttree2",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    dependent        Component depending on universal
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-cexclusive1",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    exclusive1       Component conflicting with exclusive2
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-cexclusive2",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-cexclusive1|exclusive2",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-cuniversal",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    dependent        Component depending on universal
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_component_list, db_path, "-cdependent",
                status=0,
                stdout_equals=dedent("""\
                    arch2_tree2_only Arch2&Tree2-only component
                    arch_only        Arch-only component
                    exclusive1       Component conflicting with exclusive2
                    exclusive2       Component conflicting with exclusive1
                    tree_only        Tree-only component
                    universal        A universally-supported component
                """)
            )

            self.assertKpetProduces(
                kpet_test_list, db_path,
                status=0,
                stdout_equals="arch_only\n"
                              "dependent\n"
                              "exclusive2\n"
                              "general\n"
            )

            self.assertKpetProduces(
                kpet_test_list, db_path, "-cdependent",
                status=1,
                stderr_matching=r".*Components \['dependent'\] "
                                r"are not available.*"
            )

            self.assertKpetProduces(
                kpet_test_list, db_path, "-cuniversal|dependent",
                status=0,
                stdout_equals="dependent\n"
                              "general\n"
            )

            self.assertKpetProduces(
                kpet_test_list, db_path, "-c.*",
                status=1,
                stderr_matching=r".*Components "
                                r"\['exclusive1', 'exclusive2'\] "
                                r"are not available.*"
            )

            self.assertKpetProduces(
                kpet_test_list, db_path, "-carch_only", "-aarch2",
                status=1,
                stderr_matching=r".*Components \['arch_only'\] "
                                r"are not available.*"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0,
                stdout_equals="general\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", "-carch_only",
                status=0,
                stdout_equals="general\n"
                              "arch_only\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", "-carch2_tree2_only",
                status=1,
                stderr_matching=r".*Components \['arch2_tree2_only'\] "
                                r"are not available.*"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint", "-cdependent",
                status=1,
                stderr_matching=r".*Components \['dependent'\] "
                                r"are not available.*"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-cuniversal|dependent",
                status=0,
                stdout_equals="general\n"
                              "dependent\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-cexclusive1|exclusive2",
                status=1,
                stderr_matching=r".*Components "
                                r"\['exclusive1', 'exclusive2'\] "
                                r"are not available.*"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-c.*",
                status=1,
                stderr_matching=r".*Components \['arch2_tree2_only', "
                                r"'exclusive1', 'exclusive2'\] "
                                r"are not available.*"
            )

    def test_scene(self):
        """Check components are exposed in each scene"""
        assets = {
            "index.yaml": """
                template: output.txt.j2
                arches:
                  - arch
                trees:
                  tree: {}
                host_types:
                  normal: {}
                components:
                  a:
                    description: A
                  b:
                    description: B
                  c:
                    description: C
                case:
                  name: test
                  universal_id: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
            """,
            "output.txt.j2": dedent("""\
                {%- for scene in SCENES -%}
                  {{- scene.components | string + "\\n" -}}
                {%- endfor -%}
            """)
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0,
                stdout_equals="[]\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-c", "",
                status=0,
                stdout_equals="[]\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-c.*",
                status=0,
                stdout_equals="['a', 'b', 'c']\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-ca", "-e", "-cb", "-e", "-cc",
                status=0,
                stdout_equals="['a']\n['b']\n['c']\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-c.*", "-e", "-e",
                status=0,
                stdout_equals="['a', 'b', 'c']\n"
                              "['a', 'b', 'c']\n"
                              "['a', 'b', 'c']\n"
            )

        assets = {
            "index.yaml": """
                template: output.txt.j2
                arches:
                  - arch
                trees:
                  tree: {}
                host_types:
                  normal: {}
                case:
                  name: test
                  universal_id: test
                  location: somewhere
                  maintainers:
                    - name: maint
                      email: maint@maintainers.org
            """,
            "output.txt.j2": dedent("""\
                {%- for scene in SCENES -%}
                  {{- scene.components | string + "\\n" -}}
                {%- endfor -%}
            """)
        }

        with assets_mkdir(assets) as db_path:
            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                status=0,
                stdout_equals="[]\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-c", "",
                status=0,
                stdout_equals="[]\n"
            )

            self.assertKpetProduces(
                kpet_run_generate, db_path, "--no-lint",
                "-c.*",
                status=0,
                stdout_equals="[]\n"
            )
