# Copyright (c) 2023 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Test cases for the misc module"""
import unittest

from kpet import misc


class MiscTest(unittest.TestCase):
    """Test cases for the misc module."""

    def test_unindent(self):
        """Check the unindent() function works correctly."""
        assert misc.unindent("") == ""
        assert misc.unindent("\n") == "\n"
        assert misc.unindent(" ") == ""
        assert misc.unindent(" \n ") == "\n"
        assert misc.unindent(" \n") == "\n"
        assert misc.unindent("\n ") == "\n"
        assert misc.unindent("\n  ") == "\n"
        assert misc.unindent(" \n \n") == "\n\n"
        assert misc.unindent("  \n  \n") == "\n\n"
        assert misc.unindent("  \n   \n") == "\n \n"
        assert misc.unindent(" \n\n") == "\n\n"
        assert misc.unindent("\n \n") == "\n\n"
        assert misc.unindent("\n \n ") == "\n\n"
        assert misc.unindent("\n \n  ") == "\n\n "
        assert misc.unindent("a\n b") == "a\n b"
        assert misc.unindent(" a\n b") == "a\nb"
        assert misc.unindent(" a\n  b") == "a\n b"
        assert misc.unindent(" a\nb") == "a\nb"
