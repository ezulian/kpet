# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Test cases for run module"""
import os
import shutil
import tempfile
import unittest

from kpet import data


class DataTest(unittest.TestCase):
    """Test cases for data module."""
    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()

    def test_invalid_schema(self):
        """
        Check that Object raiser Invalid exception if the schema doesn't match.
        """
        with open(os.path.join(self.tmpdir, 'index.yaml'), 'w',
                  encoding='utf8') as fhandle:
            fhandle.write('')

        with self.assertRaises(data.Invalid):
            data.Base(self.tmpdir)

    def tearDown(self):
        shutil.rmtree(self.tmpdir)

    def test_invalid_schema2(self):
        """
        Check that Object raiser Invalid exception if the schema doesn't match.
        """
        self.tmpdir = '/tmp/assets'
        path2assets = os.path.join(os.path.dirname(__file__),
                                   'assets/db/general')
        shutil.copytree(path2assets, self.tmpdir)

        case = os.path.join(self.tmpdir, 'cases/default/index.yaml')

        with open(case, 'r', encoding='utf8') as fhandle:
            mydata = fhandle.read()
            mydata = mydata.replace('maintainers:', 'maintainers: []')
            mydata = mydata.replace('- name: maint1', '')
            mydata = mydata.replace('  email: maint1@maintainers.org', '')

            # overwrite the file, without required 'maintainers: field'
            with open(case, 'w', encoding='utf8') as fhandle2:
                fhandle2.write(mydata)

        with self.assertRaises(data.Invalid):
            data.Base(self.tmpdir)


class DataPatternTest(unittest.TestCase):
    """Pattern tests"""

    # pylint: disable=invalid-name
    # (matching unittest conventions)

    def assertMatch(self, pattern_data, **target_kwargs):
        """
        Assert a pattern matches.

        Args:
            pattern_data:   The data to create the pattern from.
            target_kwargs:  Keyword arguments to create the target from.
        """
        self.assertTrue(data.Pattern(pattern_data).
                        matches(data.Target(**target_kwargs)))

    def assertMismatch(self, pattern_data, **target_kwargs):
        """
        Assert a pattern mismatches.

        Args:
            pattern_data:   The data to create the pattern from.
            target_kwargs:  Keyword arguments to create the target from.
        """
        self.assertFalse(data.Pattern(pattern_data).
                         matches(data.Target(**target_kwargs)))

    def test_empty(self):
        """Check empty patterns match anything"""
        self.assertMatch({})

        self.assertMatch({}, components={"a"})

        self.assertMatch({}, trees=None)
        self.assertMatch({}, components=set())
        self.assertMatch({}, components=set(), trees={"a"})
        self.assertMatch({}, components={"a"}, trees={"a"})

    def test_boolean(self):
        """Check how boolean Patterns match."""
        self.assertMatch(True, components=None)
        self.assertMatch(True, components=set())
        self.assertMatch(True, components={"a"})

        self.assertMismatch(False, components=None)
        self.assertMismatch(False, components=set())
        self.assertMismatch(False, components={"a"})

    def test_two_params(self):
        """Check two-parameter patterns match correctly"""
        self.assertMatch(dict(components="a", trees="A"),
                         components=None, trees=None)
        self.assertMatch(dict(components="a", trees="A"),
                         components={"a"}, trees=None)
        self.assertMatch(dict(components="a", trees="A"),
                         components={"a"}, trees={"A"})

        self.assertMismatch(dict(components="a", trees="A"),
                            components={"a"}, trees={"B"})
        self.assertMismatch(dict(components="a", trees="A"),
                            components={"b"}, trees={"A"})
        self.assertMismatch(dict(components="a", trees="A"),
                            components={"b"}, trees={"B"})

    def test_multi_value(self):
        """Check patterns match multiple values correctly"""
        self.assertMatch(dict(components="a"), components={"a", "a"})
        self.assertMatch(dict(components="a"), components={"a", "b"})
        self.assertMismatch(dict(components="a"), components={"b", "b"})
        self.assertMismatch(dict(components="b"), components={"a", "a"})
        self.assertMatch(dict(components="b"), components={"a", "b"})
        self.assertMatch(dict(components="b"), components={"b", "b"})

    def test_multi_regex(self):
        """Check multi-regex patterns match correctly"""
        self.assertMatch(dict(components={"or": ["a", "a"]}),
                         components={"a"})
        self.assertMatch(dict(components={"or": ["a", "b"]}),
                         components={"a"})
        self.assertMismatch(dict(components={"or": ["b", "b"]}),
                            components={"a"})
        self.assertMismatch(dict(components={"or": ["a", "a"]}),
                            components={"b"})
        self.assertMatch(dict(components={"or": ["a", "b"]}),
                         components={"b"})
        self.assertMatch(dict(components={"or": ["b", "b"]}),
                         components={"b"})

    def test_not(self):
        """Check negation works"""
        self.assertMismatch({"not": {}})
        self.assertMatch({"not": {"not": {}}})
        self.assertMismatch({"not": {"trees": ".*"}}, trees={"a"})
        self.assertMatch({"not": {"not": {"trees": ".*"}}}, trees={"a"})

    def test_or(self):
        """Check disjunction works"""
        # Empty dict
        self.assertMismatch({"or": {}})
        # Empty list
        self.assertMismatch({"or": []})
        # One true in dict
        self.assertMatch({"or": dict(trees=".*")}, trees={"foo"})
        # One true in list
        self.assertMatch({"or": [dict(trees=".*")]}, trees={"foo"})
        # One false in dict
        self.assertMismatch({"or": dict(trees="bar")}, trees={"foo"})
        # One false in list
        self.assertMismatch({"or": [dict(trees="bar")]}, trees={"foo"})
        # True and false in dict
        self.assertMatch({"or": dict(trees=".*", arches="bleh")},
                         trees={"foo"}, arches={"baz"})
        # True and false in list
        self.assertMatch({"or": [dict(trees=".*"), dict(arches="bleh")]},
                         trees={"foo"}, arches={"baz"})
        # Two truths in dict
        self.assertMatch({"or": dict(trees=".*", arches="baz")},
                         trees={"foo"}, arches={"baz"})
        # Two truths in list
        self.assertMatch({"or": [dict(trees=".*"), dict(arches="baz")]},
                         trees={"foo"}, arches={"baz"})
        # Two falses in dict
        self.assertMismatch({"or": dict(trees="oof", arches="zab")},
                            trees={"foo"}, arches={"baz"})
        # Two falses in list
        self.assertMismatch({"or": [dict(trees="oof"), dict(arches="zab")]},
                            trees={"foo"}, arches={"baz"})

    def test_and(self):
        """Check conjunction works"""
        # Empty dict
        self.assertMatch({"and": {}})
        # Empty list
        self.assertMatch({"and": []})
        # One true in dict
        self.assertMatch({"and": dict(trees=".*")}, trees={"foo"})
        # One true in list
        self.assertMatch({"and": [dict(trees=".*")]}, trees={"foo"})
        # One false in dict
        self.assertMismatch({"and": dict(trees="bar")}, trees={"foo"})
        # One false in list
        self.assertMismatch({"and": [dict(trees="bar")]}, trees={"foo"})
        # True and false in dict
        self.assertMismatch({"and": dict(trees=".*", arches="bleh")},
                            trees={"foo"}, arches={"baz"})
        # True and false in list
        self.assertMismatch({"and": [dict(trees=".*"), dict(arches="bleh")]},
                            trees={"foo"}, arches={"baz"})
        # Two truths in dict
        self.assertMatch({"and": dict(trees=".*", arches="baz")},
                         trees={"foo"}, arches={"baz"})
        # Two truths in list
        self.assertMatch({"and": [dict(trees=".*"), dict(arches="baz")]},
                         trees={"foo"}, arches={"baz"})
        # Two falses in dict
        self.assertMismatch({"and": dict(trees="oof", arches="zab")},
                            trees={"foo"}, arches={"baz"})
        # Two falses in list
        self.assertMismatch({"and": [dict(trees="oof"), dict(arches="zab")]},
                            trees={"foo"}, arches={"baz"})

    def test_default_op(self):
        """Check default operation (conjunction) works"""
        # Empty dict
        self.assertMatch({})
        # Empty list
        self.assertMatch([])
        # One true in dict
        self.assertMatch(dict(trees=".*"), trees={"foo"})
        # One true in list
        self.assertMatch([dict(trees=".*")], trees={"foo"})
        # One false in dict
        self.assertMismatch(dict(trees="bar"), trees={"foo"})
        # One false in list
        self.assertMismatch([dict(trees="bar")], trees={"foo"})
        # True and false in dict
        self.assertMismatch(dict(trees=".*", arches="bleh"),
                            trees={"foo"}, arches={"baz"})
        # True and false in list
        self.assertMismatch([dict(trees=".*"), dict(arches="bleh")],
                            trees={"foo"}, arches={"baz"})
        # Two truths in dict
        self.assertMatch(dict(trees=".*", arches="baz"),
                         trees={"foo"}, arches={"baz"})
        # Two truths in list
        self.assertMatch([dict(trees=".*"), dict(arches="baz")],
                         trees={"foo"}, arches={"baz"})
        # Two falses in dict
        self.assertMismatch(dict(trees="oof", arches="zab"),
                            trees={"foo"}, arches={"baz"})
        # Two falses in list
        self.assertMismatch([dict(trees="oof"), dict(arches="zab")],
                            trees={"foo"}, arches={"baz"})

    def test_any(self):
        """Check the "UNKNOWN" target sets are handled correctly"""
        # Empty pattern
        self.assertMatch({})
        self.assertMatch({}, trees=None)
        self.assertMatch({}, trees=None, components=None)

        # Basic patterns
        self.assertMatch(dict(trees="foo"), trees=None)
        self.assertMatch(dict(trees="foo", components="bar"),
                         trees=None, components={"bar"})
        self.assertMatch(dict(trees="foo", components="bar"),
                         trees={"foo"}, components=None)
        self.assertMismatch(dict(trees="foo", components="bar"),
                            trees=None, components=set())
        self.assertMismatch(dict(trees="foo", components="bar"),
                            components=set())

        # Negation
        self.assertMatch({"not": dict(trees="foo")},
                         trees=None)
        self.assertMatch({"not": {"not": dict(trees="foo")}},
                         trees=None)
        self.assertMatch({"not": dict(trees="foo", components="bar")},
                         trees=None, components={"baz"})
        self.assertMatch({"not": dict(trees="foo", components="bar")},
                         trees=None, components=set())
        self.assertMatch({"not": dict(trees="foo", components="bar")},
                         trees=None, components={"bar"})

        # Disjunction
        self.assertMatch({"or": dict(trees="foo", components="bar")},
                         trees=None, components={"baz"})
        self.assertMatch({"or": dict(trees="foo", components="bar")},
                         trees=None, components=set())
        self.assertMatch({"or": dict(trees="foo", components="bar")},
                         trees=None, components={"bar"})
        self.assertMismatch({"not":
                             {"or": dict(trees="foo", components="bar")}},
                            trees=None, components={"bar"})

        # A complex real-life case
        self.assertMatch(dict(trees={"not": {"or": ["rhel5.*"]}},
                              components={"or": [
                                  "a.*", "b.*", "c.*", "d.*", "e.*"
                              ]}),
                         trees=None,
                         arches={"x86_64"},
                         components=None)
